<html>
  <head>
    <title>PHP Test</title>
  </head>
  <body>
    <?php
    $numABuscar = (trim($_POST['num']));
    $loteria = array(61, 32, 43, 61);
    $countNums = array_count_values($loteria);
    $numDeValors = $countNums[$numABuscar];
    if( empty($numDeValors) ){
      echo "El numero $numABuscar no apareix cap vegada";
    } else {
      echo "El numero $numABuscar apareix $numDeValors vegades";
    };
?> 
  </body>
</html>